# Kali-Tweaks

Kali-Tweaks is a tool that makes it easy for users to configure their [Kali Linux](https://www.kali.org/) installation.

It provides an interactive command line interface to eases common operations.

## Installation

```
kali@kali:~$ sudo apt update && sudo apt -y install kali-tweaks
```

## Development

Install the required dependencies:

```
$ sudo apt install -y python3-newt
$ git clone https://gitlab.com/kalilinux/packages/kali-tweaks.git
$ cd kali-tweaks/
```

Run locally from the source tree:

```
$ export RUN_FROM_SOURCE_TREE=1
$ python3 -m kali_tweaks
```

To enable debug for logs:

```
$ export DEBUG=1
$ python3 -m kali_tweaks
```

To run various tests on the code:

```
$ tox
```

Notes for developers:
- kali-tweaks should be functional on a 80 columns terminal. Try to be concise
  in the settings descriptions.
