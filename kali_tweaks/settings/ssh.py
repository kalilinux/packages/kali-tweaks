# Copyright 2022 Offensive Security
# SPDX-license-identifier: GPL-3.0-only

import os

from kali_tweaks.utils import (
    run_as_root,
    say,
)


class SSHSetting:
    def __init__(self):
        self.factory_dir = "/usr/share/kali-defaults"
        self.kali_conf = "/etc/ssh/ssh_config.d/kali-wide-compat.conf"

    def load(self):
        fn = self.kali_conf
        if os.path.isfile(fn):
            value = "compat"
        else:
            value = "secure"
        return {"hardening": value}

    def apply(self, config):
        say("Configuring SSH")
        if "hardening" in config:
            value = config["hardening"]
            if value == "compat":
                self.set_wide_compat()
            elif value == "secure":
                self.unset_wide_compat()
            else:
                raise NotImplementedError(f"'{value}' not supported")

    def set_wide_compat(self):
        factory = self.factory_dir
        fn = self.kali_conf
        print("> Enabling wide compatibility")
        run_as_root(f"cp -f {factory}{fn} {fn}", log=True)

    def unset_wide_compat(self):
        fn = self.kali_conf
        print("> Disabling wide compatibility")
        run_as_root(f"rm -f {fn}", log=True)
